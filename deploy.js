const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const { interface, bytecode } = require('./compile');
require('dotenv').config()

const MNEMONIC = process.env.MNEMONIC.split('_').join(' ');
const INFURA_RINKEBY_ENDPOINT = process.env.INFURA_RINKEBY_ENDPOINT

const provider = new HDWalletProvider(
  MNEMONIC,
  INFURA_RINKEBY_ENDPOINT
);
const web3 = new Web3(provider);

const deploy = async () =>{
  const accounts = await web3.eth.getAccounts();

  console.log('Attempting to deploy from account', accounts[0]);

  const result = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: '0x'+ bytecode})
    .send({gas: '3000000', from: accounts[0] });
  console.log('contract deployed to', result.options.address);
};

deploy();