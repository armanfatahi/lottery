// solhint-disable-next-line compiler-fixed, compiler-gt-0_4
pragma solidity ^0.4.22;


contract Lottery {
    address public manager;
    address[] public players;
    
    constructor() public {
        manager = msg.sender;
    }
    
    function enter() public payable {
        require(msg.value > .01 ether); // solhint-disable-line
        
        players.push(msg.sender);
    }
    
    function pickWinner() public restricted {
        
        
        uint index = random() % players.length;
        players[index].transfer(address(this).balance);
        players = new address[](0); // solhint-disable-line
    }

    function getPlayers() public view returns (address[]) {
        return players;
    }

    function random() private view returns(uint) {
        return uint(keccak256(abi.encodePacked(block.difficulty, now, players))); // solhint-disable-line
    }
    
    modifier restricted() {
        require(msg.sender == manager);
        _;
    }
    
}